<?php
date_default_timezone_set('America/Mexico_City');
/*
----- funciones
*/

function chars_specials( $str ){

  $str = str_replace( '&quot;', '"', $str ); 
  $str = str_replace( '&nbsp;', ' ', $str );  
  $str = str_replace( '&ldquo;', '', $str );
  $str = str_replace( '&rdquo;', '', $str );
  $str = str_replace( "&lsquo;", "'", $str );
  $str = str_replace( "&rsquo;", "'", $str ); 
  
  return $str; 
} 

function convert_chars_to_entities( $str ) 
{ 
    $str = str_replace( 'ÃƒÂ¡', '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ©', '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ­', '&iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ³', '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂº', '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ±', '&ntilde;', $str ); 
    //$str = str_replace( '"', '&quot;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â¡' , '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â©' , '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â³' , '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Âº' , '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ¢Â€Â°', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â±', '&ntilde;', $str ); 
    $str = str_replace( 'ÃƒÂƒ-', '&iacute;', $str ); 
    $str = str_replace( 'ci?', 'ci&oacute;n', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‰', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ“', '&Oacute;', $str ); 
    $str = str_replace( 'ÃƒÂš', '&Uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‘', '&Ntilde;', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'a&ntilde;a', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'a&ntilde;e', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'a&ntilde;i', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'a&ntilde;o', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'a&ntildeu', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'e&ntilde;a', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'e&ntilde;e', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'e&ntildei', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'e&ntildeu', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'i&ntilde;a', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'i&ntilde;e', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'i&ntilde;i', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'i&ntilde;o', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'i&ntilde;u', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'o&ntilde;a', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'o&ntilde;e', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'o&ntilde;i', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'o&ntilde;u', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'u&ntilde;a', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'u&ntilde;e', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'u&ntilde;i', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'u&ntilde;o', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'u&ntilde;u', $str );
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½n', 'i&oacute;n', $str );
    $str = str_replace( 'Ã¡', '&aacute;', $str ); 
    $str = str_replace( 'Ã©', '&eacute;', $str ); 
    $str = str_replace( 'Ã­', '&iacute;', $str ); 
    $str = str_replace( 'Ã³', '&oacute;', $str ); 
    $str = str_replace( 'Ãº', '&uacute;', $str ); 
    $str = str_replace( 'Ã±', '&ntilde;', $str );
    $str = str_replace( 'Ã¼', '&uuml;', $str );  //#latin small letter "ü" 
    $str = str_replace( 'Ã', '&Aacute;', $str ); //Ã con tilde
    $str = str_replace( 'Ãš', '&Uacute;', $str ); //U con tilde   

   
    return $str; 
}

function complete_url($img_size,$url){
    $img_url= strpos($img_size, 'http://');
    if($img_url === false){
        $new_url= $url.$img_size;
    }else{
        $new_url= $img_size;
    }
    return($new_url);
}

if(isset($_REQUEST['tipo']) && $_REQUEST['tipo'] != ""){
    $tipo = $_REQUEST['tipo'];    
}else{
    $tipo = "article";
}

if(isset($_REQUEST['formatvid']) && $_REQUEST['formatvid'] != ""){
    $formatvid = $_REQUEST['formatvid'];    
}else{
    $formatvid = "apps";
}

$req_article = 'http://deportes.televisa.com/content/televisa/deportes.mix.js';
$req_photo = 'http://deportes.televisa.com/content/televisa/deportes.mphoto.js';
$req_video = 'http://deportes.televisa.com/content/televisa/deportes.mvideo.js';


$url_feed_article = explode("content/televisa", $req_article);
$url_article = trim($url_feed_article[0], "/");

$url_feed_photo = explode("content/televisa", $req_photo);
$url_photo = trim($url_feed_photo[0], "/");

$url_feed_video = explode("content/televisa", $req_video);
$url_videos = trim($url_feed_video[0], "/");


if(isset($_REQUEST['formatvid']) && $_REQUEST['formatvid'] != ""){
    $formatvid = $_REQUEST['formatvid'];    
}else{
    $formatvid = "apps";
}

$subdominio= array("m4v.","media.","flash.");
$fechas = array();
$cURL_article = curl_init($req_article);
curl_setopt($cURL_article,CURLOPT_RETURNTRANSFER, TRUE);
$app_arti = curl_exec($cURL_article);
//$app_arti = utf8_encode(curl_exec($cURL_article));
$deportes_article = json_decode($app_arti, true);                    //echo "<pre>"; print_r($deportes_article); echo "</pre>";die();

$cURL_photo = curl_init($req_photo);
curl_setopt($cURL_photo,CURLOPT_RETURNTRANSFER, TRUE);
$app_pho = curl_exec($cURL_photo);
//$app_pho = utf8_encode(curl_exec($cURL_photo));
$deportes_photo = json_decode($app_pho, true);                    //echo "<pre>"; print_r($deportes_photo); echo "</pre>";die();

$cURL_video = curl_init($req_video);
curl_setopt($cURL_video,CURLOPT_RETURNTRANSFER, TRUE);
$app_vi = curl_exec($cURL_video);
//$app_vi = utf8_encode(curl_exec($cURL_video));
$deportes_video = json_decode($app_vi, true);              //echo "<pre>"; print_r($deportes_video); echo "</pre>";die();

$array_feed_article= array();
$array_feed_video= array();
$array_feed_photo= array();	

if(isset($deportes_article['items'][0]['pubDate']) && $deportes_article['items'][0]['pubDate'] <> "" && $deportes_article['items'][0]['pubDate'] <> NULL){
  $fecha_ar = date_create($deportes_article['items'][0]['pubDate']);
  $fecha_article = date_format($fecha_ar, 'Y-m-d H:i:s');
} else {

  $fecha_article = date("Y-m-d H:i:s");
  #$fecha_article = date ( 'Y-m-d H:i:s' , $fecha_ar );
}


if(isset($deportes_photo['items'][0]['pubDate']) && $deportes_photo['items'][0]['pubDate'] <> "" && $deportes_photo['items'][0]['pubDate'] <> NULL){
  $fecha_ph = date_create($deportes_photo['items'][0]['pubDate']);
  $fecha_photo = date_format($fecha_ph, 'Y-m-d H:i:s');
} else {

  $fecha_ph = date("Y-m-d H:i:s");
  $fecha_photo = strtotime ( '-3 day' , strtotime ( $fecha_ph ) ) ;
  $fecha_photo = date ( 'Y-m-d H:i:s' , $fecha_photo );
}

if(isset($deportes_video['items'][0]['publidate']) && $deportes_video['items'][0]['publidate'] <> "" && $deportes_video['items'][0]['publidate'] <> NULL){
  $fecha_vi = date_create($deportes_video['items'][0]['publidate']);
  $fecha_video = date_format($fecha_vi, 'Y-m-d H:i:s');
} else {

  $fecha_vi = date("Y-m-d H:i:s");
  $fecha_video = strtotime ( '-4 day' , strtotime ( $fecha_vi ) ) ;
  $fecha_video = date ( 'Y-m-d H:i:s' , $fecha_video );
}

	#echo "fecha es article " . $fecha_article . "<br />";
	#echo "fecha es photo " . $fecha_photo . "<br />";
	#echo "fecha es video " . $fecha_video .  "<br /><br /><br />";
	#die();

if($fecha_article <> $fecha_photo && $fecha_article <> $fecha_video && $fecha_photo <> $fecha_video){

	$fechas[0]['fecha']=$fecha_article;
	$fechas[0]['tipo']='article';
	$fechas[1]['fecha']=$fecha_photo;
	$fechas[1]['tipo']='photo';
	$fechas[2]['fecha']=$fecha_video;
	$fechas[2]['tipo']='video';
	rsort($fechas);

	#echo "<pre>"; print_r($fechas); echo "</pre>";
	#die();

} else {
	$fechas[0]['fecha']=$fecha_article;
	$fechas[0]['tipo']='article';
}
//echo "antes de validar MXM: ".$fechas[0]['tipo']."<br><br>";
//Validando Notas MXM (Tipo Articulo)
if($fechas[0]['tipo'] == 'article'){
    $auxurl_mxm= $deportes_article['items'][0]['link'];                 //echo $auxurl_mxm."<br>";
    $aux_mxm_search= strpos($auxurl_mxm, "/mxm/futbol/");
    $aux_live_search= strpos($auxurl_mxm, "/video/vivo/");
	$aux_videoencuesta_search= strpos($auxurl_mxm, "videoencuesta");
    
    //-------------------       Validando notas mxm
    if($aux_mxm_search !== false){
        $content_art= $deportes_article['items'][0]['content'];
        $size_content= strlen($content_art);
        if($content_art == "" || $size_content < 100){
            $fechas[0]['tipo']='video';
        }    
    }
    
    //-------------------       Validando notas en vivo (live)
    if($aux_live_search !== false){
            $fechas[0]['tipo']='video';
    }
	
    //-------------------       Validando videoencuesta
    if($aux_videoencuesta_search !== false){
            $fechas[0]['tipo']='video';
    }	
	
} 
                                                                        //echo "despues de validar MXM: ".$fechas[0]['tipo']."<br><br>";
//die();

if($fechas[0]['tipo'] == 'article'){
  
	   $array_feed_article[0]['title']= $deportes_article['items'][0]['title'];
	   $resultado = strpos($deportes_article['items'][0]['link'], 'http://');
	   if($resultado !== false){
			$link = $deportes_article['items'][0]['link']; 
	   } else {
			$link = $url_article.$deportes_article['items'][0]['link'];
	   } 		   
	   $array_feed_article[0]['link'] = $link;
	   $array_feed_article[0]['authorname'] = $deportes_article['items'][0]['authorname'];
	   $array_feed_article[0]['description'] = $deportes_article['items'][0]['description'];
	   if (isset($deportes_article['items'][0]['pubDate']) && $deportes_article['items'][0]['pubDate'] != ""){
		   $array_feed_article[0]['pubDate'] = date('D, d M Y H:i:s ',strtotime($deportes_article['items'][0]['pubDate']));  
	   } else {
		   $array_feed_article[0]['pubDate'] = date("D, d M Y H:i:s");  
	   }	   
	   $thumb = complete_url($deportes_article['items'][0]['thumbnail'], $url_article);   
	   $array_feed_article[0]['thumb'] = $thumb;
	   
	   $i1024 = complete_url($deportes_article['items'][0]['images']['1024x768'],$url_article);		   
	   $i110 = complete_url($deportes_article['items'][0]['images']['110x110'], $url_article);
	   $i136 = complete_url($deportes_article['items'][0]['images']['136x102'], $url_article);
	   $i104 = complete_url($deportes_article['items'][0]['images']['136x104'], $url_article);
	   $i77 = complete_url($deportes_article['items'][0]['images']['136x77'], $url_article);
	   $i192 = complete_url($deportes_article['items'][0]['images']['192x107'], $url_article);
	   $i210 = complete_url($deportes_article['items'][0]['images']['210x120'], $url_article);   
	   $i300 = complete_url($deportes_article['items'][0]['images']['300x169'], $url_article);
	   $i319 = complete_url($deportes_article['items'][0]['images']['319x319'], $url_article);
	   $i408 = complete_url($deportes_article['items'][0]['images']['408x306'], $url_article);
	   $i48 = complete_url($deportes_article['items'][0]['images']['48x48'], $url_article);
	   $i624 = complete_url($deportes_article['items'][0]['images']['624x351'], $url_article);
	   $i468 = complete_url($deportes_article['items'][0]['images']['624x468'], $url_article);
	   $i63 = complete_url($deportes_article['items'][0]['images']['63x63'], $url_article);
	   $i84 = complete_url($deportes_article['items'][0]['images']['84x63'], $url_article);
	   $i47 = complete_url($deportes_article['items'][0]['images']['84x47'], $url_article);
	   $i67 = complete_url($deportes_article['items'][0]['images']['67x38'], $url_article);
	   
	   $array_feed_article[0]['images']['1024x768'] = $i1024;	   
	   $array_feed_article[0]['images']['110x110'] = $i110;   
	   $array_feed_article[0]['images']['136x102'] = $i136;
	   $array_feed_article[0]['images']['136x104'] = $i104; 
	   $array_feed_article[0]['images']['136x77'] = $i77;		   
	   $array_feed_article[0]['images']['192x107'] = $i192;
	   $array_feed_article[0]['images']['210x120'] = $i210 ;
	   $array_feed_article[0]['images']['300x169'] = $i300;
	   $array_feed_article[0]['images']['319x319'] = $i319;
	   $array_feed_article[0]['images']['408x306'] = $i408;
	   $array_feed_article[0]['images']['48x48'] = $i48;
	   $array_feed_article[0]['images']['624x351'] = $i624;
	   $array_feed_article[0]['images']['624x468'] = $i468;		   
	   $array_feed_article[0]['images']['63x63'] = $i63;
	   $array_feed_article[0]['images']['84x63'] = $i84;
	   $array_feed_article[0]['images']['84x47'] = $i47;
	   $array_feed_article[0]['images']['67x38'] = $i67;

	   $array_feed_article[0]['content'] = $deportes_article['items'][0]['content'];
	   $array_feed_article[0]['guid'] = $deportes_article['items'][0]['guid'];
	   if($deportes_article['items'][0]['keywords'] != "" || $deportes_article['items'][0]['keywords'] != NULL){
		 $keywords = $deportes_article['items'][0]['keywords'];
	   }else{
		 $keywords = 'Televisa Deportes';
	   }		   
	   $array_feed_article[0]['keywords'] = $keywords;
	   $array_feed_article[0]['comments'] = $deportes_article['items'][0]['comments'];
	   $array_feed_article[0]['category'] = $deportes_article['items'][0]['category'];
	   
  
}elseif($fechas[0]['tipo'] == 'photo'){

	   $array_feed_photo[0]['title']= $deportes_photo['items'][0]['title'];
	   $array_feed_photo[0]['typeElement']= $deportes_photo['items'][0]['typeElement'];
	   
	   $rlink = strpos($deportes_photo['items'][0]['link'], 'http://');
	   if($rlink !== false){
			$link = $deportes_photo['items'][0]['link']; 
	   } else {
			$link = $url_photo.$deportes_photo['items'][0]['link'];
	   }  			   
	   $array_feed_photo[0]['link'] = $link;
	   if( isset($deportes_photo['items'][0]['description']) && $deportes_photo['items'][0]['description'] != ""){
	     //$array_feed_photo[0]['description'] = $deportes_photo['items'][0]['description'];
         $porciones = explode(" ", $deportes_photo['items'][0]['description']);
	     $p="";
         foreach($porciones as $porcion){
            $v = convert_chars_to_entities($porcion);
            $p = $p . " ". $v;
         }
         $array_feed_photo[0]['description'] = $p;
	   } else {
	     $array_feed_photo[0]['description'] = $deportes_photo['items'][0]['title'];
	   }
	   
	   if ( !isset($deportes_photo['items'][0]['pubDate']) || $deportes_photo['items'][0]['pubDate'] == '' || $deportes_photo['items'][0]['pubDate'] == NULL || empty($deportes_photo['items'][0]['pubDate']) ){
		   $pubDate = date('D, d M Y H:i:s ',strtotime(date("Y/m/d")));		
	   } else {
		   $pubDate = date('D, d M Y H:i:s ',strtotime($deportes_photo['items'][0]['pubDate']));
	   }			   
	   $array_feed_photo[0]['pubDate'] = $pubDate;
	   $rthum = strpos($deportes_photo['items'][0]['thumbnail'], 'http://');
	   if($rthum !== false){
			$thumb = $deportes_photo['items'][0]['thumbnail']; 
	   } else {
			$thumb = $url.$deportes_photo['items'][0]['thumbnail'];
	   } 			   
	   $array_feed_photo[0]['thumbnail'] = $thumb;
	   $array_feed_photo[0]['photos'] = $deportes_photo['items'][0]['photo'];
	   $array_feed_photo[0]['guid'] = $deportes_photo['items'][0]['guid'];

}elseif($fechas[0]['tipo'] == 'video' && $deportes_video['items'][0]['urls'][$formatvid] != "" && $deportes_video['items'][0]['urls'][$formatvid] != ""){
   
		$array_feed_video[0]['title']= $deportes_video['items'][0]['title'];
		
		$url_video = $deportes_video['items'][0]['urls'][$formatvid];                       
		//$url_video= $val['urls']["apps"];                    
		$ur_vid_aux = explode("?", $url_video);
		if(count($ur_vid_aux > 1)){
			$url_video = $ur_vid_aux[0];
		}
		$url_video = strtolower($url_video);
		$url_video = str_replace($subdominio, 'apps.',$url_video);	

		if(isset($deportes_video['items'][0]['description']) && $deportes_video['items'][0]['description'] != ""){
			$description= $deportes_video['items'][0]['description'];
		}else{
			$description= $deportes_video['items'][0]['title'];
		}
		if(isset($deportes_video['items'][0]['date']) && $deportes_video['items'][0]['date'] != ""){
			$date= $deportes_video['items'][0]['date'];
		}else{
			$date= date("y-m-d");
		}
		
		if(isset($deportes_video['items'][0]['duration']) && $deportes_video['items'][0]['duration'] != ""){
			$duration= $deportes_video['items'][0]['duration'];
		}else{
			$duration= "10";
		}

		$rlink = strpos($deportes_video['items'][0]['urlpublic'], 'http://');
		if($rlink !== false){
			$link = $deportes_video['items'][0]['urlpublic']; 
		} else {
			$link = $url_videos.$deportes_video['items'][0]['urlpublic'];
		} 				
		$array_feed_video[0]['link']= $link;
		
		$array_feed_video[0]['description']= $description;
		$array_feed_video[0]['external_id']= $deportes_video['items'][0]['id'];		
		
		if(isset($deportes_video['items'][0]['publidate']) && $deportes_video['items'][0]['publidate'] != ""){
			$date_pub= date('D, d M Y H:i:s ',strtotime($deportes_video['items'][0]['publidate']));
		}else{
			$date_pub= date("D, d M Y H:i:s");
		}      	    
		$array_feed_video[0]['pubDate']= $date_pub;
		$array_feed_video[0]['pubDateTime']= $date_pub;
		
		if($url_video <> ""){ 
		  $array_feed_video[0]['url_video']= $url_video;
		} 
		$array_feed_video[0]['duration']= $duration;
		$array_feed_video[0]['program']= 'deportes';
		$array_feed_video[0]['role']= 'deportes';
		
		$partes = explode("/",$deportes_video['items'][0]['path']);
		$path = $partes[0]."/".$partes[1]."/".$partes[2]."/".$partes[3]."/".$partes[4]."/";
		$array_feed_video[0]['path']= $path;
		
		if ($deportes_video['items'][0]['keywords'] != "" || $deportes_video['items'][0]['keywords'] != NULL){
		  $keywords = $deportes_video['items'][0]['keywords'];
		}else{
		  $keywords = "Televisa Deportes";
		}
		$array_feed_video[0]['keywords']= $keywords;
		
		$rthumb = strpos($deportes_video['items'][0]['thumb'], 'http://');
		if($rthumb !== false){
		   $thumb = $deportes_video['items'][0]['thumb']; 
		} else {
		   $thumb = $url_videos.$deportes_video['items'][0]['thumbnail'];
		}
        $sepurl_thumb= explode("?", $thumb);
            if(count($sepurl_thumb) > 1){
                $thumb= $sepurl_thumb[0];
                $thumb= str_replace($subdominio, 'apps.',$thumb); 
        }			
		$array_feed_video[0]['thumbnail']= $thumb;
		
		$resultado = strpos($deportes_video['items'][0]['still'], 'http://');
		if($resultado !== false){
			$still = $deportes_video['items'][0]['still']; 
		} else {
		   $still = $url_videos.$deportes_video['items'][0]['still'];
		}
        $sepurl_still= explode("?", $still);        //limpiando url
        if(count($sepurl_still) > 1){
            $still= $sepurl_still[0];
            $still= str_replace($subdominio, 'apps.',$still); 
        }
		$array_feed_video[0]['still']= $still;
		
		$array_feed_video[0]['programid']= "0129";
		$array_feed_video[0]['guid']= $deportes_video['items'][0]['guid'];	

}else{
       $array_feed_article = 0;
	   $array_feed_photo = 0;
	   $array_feed_video = 0;
}

#echo "<pre>"; print_r($array_feed_article); echo "</pre>"; die();
#echo "<pre>"; print_r($array_feed_photo); echo "</pre>"; die();
#echo "<pre>"; print_r($array_feed_video); echo "</pre>"; die();
    
header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'> <rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom">';
echo "\n";

	   	echo "<channel>";
	   	echo "<title>televisa.com</title>";
	   	echo "<link>http://www.televisa.com</link>";
	   	echo "<description>".utf8_decode('El sitio número de internet de habla hispana con el mejor contenido de noticias, espectáculos, telenovelas, deportes, futbol, estadísticas y mucho más.')."</description>";
	   	echo "<image>";
	   	    echo "<title>televisa.com</title>";
	   	    echo "<url>http://i.esmas.com/img/univ/portal/rss/feed_1.jpg</url>";
	   	    echo "<link>http://www.televisa.com</link>";
	       echo "</image>";
	       echo "<language>es-mx</language>";
	   	echo "<copyright>2005 Comercio Mas S.A. de C.V</copyright>";
	   	echo "<managingEditor>ulises.blanco@esmas.net (Ulises Blanco)</managingEditor>";
	   	echo "<webMaster>feeds@esmas.com (feeds Esmas.com)</webMaster>";
	   	echo "<pubDate>".date("D, d M Y H:i:s")." GMT</pubDate>";
	   	echo "<lastBuildDate>".date("D, d M Y H:i:s")." GMT</lastBuildDate>";
	   	echo "<category>Home Principal esmas</category> ";
	   	echo "<generator>GALAXY 1.0</generator>";
	   	echo '<atom:link href="http://feeds.esmas.com/data-feeds-esmas/xml/index.xml" rel="self" type="application/rss+xml" />';
	   	echo "<ttl>60</ttl> ";

?>
<?php 
if (count($array_feed_article) <> 0 && $tipo == "article" && $fechas[0]['tipo'] == 'article'){
  foreach($array_feed_article as $key_item => $value_element){?>
   <item>
    	<title><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['title']); ?>]]></title>
   		<link><?=$value_element['link'];?></link>
   		<author><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['authorname']);?>]]></author>
   		<description><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT", $value_element['description']);?>]]></description>   		
   		<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
   	 	<media:thumbnail url='<?=$value_element['thumb'];?>'/>
   	 	<media:group>
   	 		 <media:content url='<?=$value_element['images']['1024x768'];?>' medium='image' height='768' width='1024' />
   	 		 <media:content url='<?=$value_element['images']['110x110'];?>' medium='image' height='110' width='110' />
   	 		 <media:content url='<?=$value_element['images']['136x102'];?>' medium='image' height='102' width='136' />
   	 		 <media:content url='<?=$value_element['images']['136x104'];?>' medium='image' height='104' width='136' />
   	 		 <media:content url='<?=$value_element['images']['136x77'];?>' medium='image' height='77' width='136' />
   	 		 <media:content url='<?=$value_element['images']['192x107'];?>' medium='image' height='107' width='192' />
   	 		 <media:content url='<?=$value_element['images']['210x120'];?>' medium='image' height='120' width='210' />
   	 		 <media:content url='<?=$value_element['images']['300x169'];?>' medium='image' height='169' width='300' />
   	 		 <media:content url='<?=$value_element['images']['319x319'];?>' medium='image' height='319' width='319' />
   	 		 <media:content url='<?=$value_element['images']['408x306'];?>' medium='image' height='306' width='400' />
   	 		 <media:content url='<?=$value_element['images']['48x48'];?>' medium='image' height='48' width='48' />
   	 		 <media:content url='<?=$value_element['images']['624x351'];?>' medium='image' height='351' width='624' />
   	 		 <media:content url='<?=$value_element['images']['624x468'];?>' medium='image' height='468' width='624' />
   	 		 <media:content url='<?=$value_element['images']['63x63'];?>' medium='image' height='63' width='63' />
   	 		 <media:content url='<?=$value_element['images']['84x63'];?>' medium='image' height='63' width='84' />
   	 		 <media:content url='<?=$value_element['images']['84x47'];?>' medium='image' height='74' width='84' />
   	 		 <media:content url='<?=$value_element['images']['67x38'];?>' medium='image' height='38' width='67' />
   	 	</media:group>
   	 	<content:encoded><![CDATA[<?=utf8_decode(chars_specials(html_entity_decode($value_element['content'])))?>]]></content:encoded>
        <guid isPermaLink='false'><?=$value_element['guid']; ?></guid>
   	 	<media:keywords><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['keywords']);?>]]></media:keywords>
   	 	<comments>http://comentarios.esmas.com/feeds/comentarios.php?url=<?=$value_element['link']; ?></comments>
   	 	<category><?=$value_element['category'];?></category>
   </item>           
<?php    
  }//for article
}
if (count($array_feed_video) <> 0 && $tipo == "video" && $fechas[0]['tipo'] == 'video'){
  foreach($array_feed_video as $key_item => $value_element){
	if($value_element['url_video'] != ""){ ?>
    <item>
        <title><![CDATA[<?=utf8_decode($value_element['title']);?>]]></title>
    	<link><?=$value_element['link'];?></link>
        <description><![CDATA[<?=utf8_decode($value_element['description'])?>]]></description>
    	<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
    	<pubDateTime><?=trim($value_element['pubDateTime']);?> CDT</pubDateTime> 
    	<media:content url='<?=$value_element['url_video']?>' fileSize='' type='video' medium='video' expression='full' duration='<?php echo $value_element['duration']?>' />
    	<media:title type='plain'><![CDATA[<?=utf8_decode($value_element['title']);?>]]></media:title>
    	<media:credit role='author' scheme='<?=$value_element['role'];?>'><![CDATA[<?php echo $value_element['path'];?>]]></media:credit>
    	<media:description type='plain'><![CDATA[<?=utf8_decode($value_element['description']);?>]]></media:description>
        <media:group>
            <media:content url='<?=$value_element['still'];?>' medium='image' height='360' width='640' />
    	</media:group>
        <media:keywords><![CDATA[<?=utf8_decode($value_element['keywords']);?>]]></media:keywords>
        <media:thumbnail url='<?=$value_element['thumbnail']?>'/>
    	<media:text>http://comentarios.esmas.com/feeds/comentarios.php?url=<?=$value_element['link']?></media:text>
    	<guid isPermaLink='false'><?=$value_element['guid']?></guid>			
    	<category><![CDATA[<?= $value_element['path'];?>]]></category>
    	<categoryid><?=$value_element['programid']?></categoryid>
    	<comments>http://comentarios.esmas.com/feeds/comentarios.php?url=<?php echo $value_element['link']?></comments>
    </item> 
    
<?php	
    }
  }//for video
}//if

if (count($array_feed_photo) <> 0 && $tipo == "photo" && $fechas[0]['tipo'] == 'photo'){
  foreach($array_feed_photo as $key_item => $value_element){
    if(isset($value_element['photos']) && count($value_element['photos']) > 0 && $value_element['photos'] <> 'empty' && $value_element['photos'] <> '' && $value_element['photos'] <> NULL ){ 
?>  
		<item>
	        <title><![CDATA[<?=utf8_decode(convert_chars_to_entities($value_element['title'])); ?>]]></title>
            <link><?=$value_element['link']?></link>
            <description><![CDATA[<?=html_entity_decode(convert_chars_to_entities(utf8_decode($value_element['description'])))?>]]></description>
			<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
			<media:thumbnail url='<?=$value_element['thumbnail']?>' /> 
<?php 
foreach($value_element['photos'] as $k => $valor){
if(isset($valor['images']) && count($valor['images']) > 0){    
  if(isset($valor['title']) &&  $valor['title'] <> "" &&  $valor['title'] <> NULL){
     $title_gal = $valor['title'];
  }else{
     $title_gal = "...";
  }?>	 
            <media:group>
	          <media:title><![CDATA[<?=chars_specials(utf8_decode($title_gal))?>]]></media:title>			 
<?php    	    
                 $images = $valor['images'];
			    foreach($images as $m => $imagen){
				   $p = strpos($m, 'x', 0);
				   $h = substr($m,$p+1); 
				   $w = substr($m,0,$p);
			       $rimg = strpos($images[$m], 'http://');
			       if($rimg !== false){ $imagen = $images[$m]; } else {  $imagen = $url.$images[$m]; }?>
                    <media:content url='<?=$imagen?>' medium='image' width='<?=$w?>' height='<?=$h?>' />
                <?php
				 }
				?>			 
			</media:group>	
<?php }
}
?>		<guid isPermaLink='false'><?=$value_element['guid']?></guid>			 
        </item>		
<?php
    }	
  }
}//if foto
  
	echo "</channel>";
	echo "</rss>";
?>