<?php
date_default_timezone_set('America/Mexico_City');
//error_reporting(E_ALL ^ E_NOTICE);
if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];    
}else{
    $seccion = "home";
}
if(isset($_REQUEST['formatvid']) && $_REQUEST['formatvid'] != ""){
    $formatvid = $_REQUEST['formatvid'];    
}else{
    $formatvid = "apps";
}
switch ($seccion) {
	case 'home':
		$req_mix = 'http://deportes.televisa.com/content/televisa/deportes.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes.mvideo.js';
        break;
	case 'futbol':
		$req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol.video.js';
		break;
    case 'seleccion':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/seleccion-mexicana.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/seleccion-mexicana.video.js';
		break;
    case 'fut_mexicano':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol-mexicano.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol-mexicano.video.js';
		break;
    case 'fut_internacional':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol-internacional.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol-internacional.video.js';
		break;       
    case 'boxeo':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/boxeo.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/boxeo.video.js';
		break; 
    case 'nba':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/nba-basquetbol.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/nba-basquetbol.video.js';
		break; 
    case 'beisbol':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/beisbol.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/beisbol.video.js';
		break; 
    case 'nfl':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/nfl-futbol-americano.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/nfl-futbol-americano.video.js';
		break; 
    case 'mas_deporte':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/mas-deportes.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/mas-deportes.video.js';
		break; 
    case 'tdn':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tdn.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tdn.video.js';
		break; 
    case 'autos':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/autos.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/autos.video.js';
		break;     
    case 'golf':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/golf.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/golf.video.js';
		break;     
    case 'tenis':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tenis.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tenis.video.js';
		break;     
    case 'tdw':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tdw.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tdw.video.js';
		break;                
}

$dom_feed= explode("content/televisa", $req_mix);
$dominio= trim($dom_feed[0], "/");
$subdominio= array("m4v.","media.","flash.");

$cURL = curl_init($req_mix);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$app_deo = curl_exec($cURL);
//$app_deo = utf8_encode(curl_exec($cURL));
$deportes_app = json_decode($app_deo, true);                                //echo "<pre>"; print_r($deportes_app); echo "</pre>";die();

$cURL_car = curl_init($req_carousel);
curl_setopt($cURL_car,CURLOPT_RETURNTRANSFER, TRUE);
$app_car = curl_exec($cURL_car);
//$app_car = utf8_encode(curl_exec($cURL_car));
$deportes_app_car = json_decode($app_car, true);                            //echo "<pre>"; print_r($deportes_app_car); echo "</pre>";die();

$array_feed= array();
$i= 0;
//RECORRIENDO MIX
if($deportes_app['items'] != null || $deportes_app['items'] != ""){         
    foreach($deportes_app['items'] as $key => $val){
        if($val["typeElement"] == "video"){
            if(isset($val['urls'][$formatvid]) && $val['urls'][$formatvid] != ""){
                $url_video= $val['urls'][$formatvid];                       
                //$url_video= $val['urls']["apps"];                    
            	$ur_vid_aux= explode("?", $url_video);
                if(count($ur_vid_aux > 1)){
                    $url_video= $ur_vid_aux[0];
                }
                $url_video= strtolower($url_video);
                $url_video= str_replace($subdominio, 'apps.',$url_video);
                
                if(isset($val['description']) && $val['description'] != ""){
            		$description= $val['description'];
            	}else{
            		$description= $val['title'];
            	}
                if(isset($val['publidate']) && $val['publidate'] != ""){
            		$date_pub= date('D, d M Y H:i:s ',strtotime($val['publidate']));
                    $pubdate_unixt= strtotime($val['publidate']);
            	}else{
            		$date_pub= date("D, d M Y H:i:s");
                    $pubdate_unixt= time();
            	}
                
                
                if(isset($val['duration']) && $val['duration'] != ""){
            		$duration= $val['duration'];
            	}else{
            		$duration= "10";
            	}
                if(isset($val['still']) && $val['still'] != ""){
            		$still= $val['still'];
            	}else{
            		$still= $val['thumb'];
            	}
                $val_still= strpos($still, 'http://');
                if($val_still === false){
                    $still= $dominio.$still;
                }
                $sepurl_still= explode("?", $still);
                if(count($sepurl_still) > 1){
                    $still= $sepurl_still[0];
                    $still= str_replace($subdominio, 'apps.',$still); 
                }
                
                $thumb= $val['thumb'];
                $val_thumb= strpos($thumb, 'http://');
                if($val_thumb === false){
                    $thumb= $dominio.$thumb;
                }
                $sepurl_thumb= explode("?", $thumb);
                if(count($sepurl_thumb) > 1){
                    $thumb= $sepurl_thumb[0];
                    $thumb= str_replace($subdominio, 'apps.',$thumb); 
                }
				
				$array_feed[$i]['unixtime']= $pubdate_unixt;
                $array_feed[$i]['section']= "mix";
                $array_feed[$i]['ind_feed']= $key;
                $array_feed[$i]['title']= $val['title'];
                $array_feed[$i]['link']=  $val['urlpublic'];
                $array_feed[$i]['description']= $description;
                $array_feed[$i]['pubDate']= $date_pub;
                $array_feed[$i]['pubDateTime']= $date_pub;
                $array_feed[$i]['url_video']= $url_video;
                $array_feed[$i]['duration']= $duration;
                $array_feed[$i]['program']= 'deportes';
                $array_feed[$i]['role']= 'deportes';
                $array_feed[$i]['path']= $val['path'];
                $array_feed[$i]['keywords']= $val['keywords'];
                $array_feed[$i]['thumbnail']= $thumb;
                $array_feed[$i]['still']= $still;
                $array_feed[$i]['programid']= "0129";
                $array_feed[$i]['guid']= $val['guid'];
                $i++;
            }
        }    
    }
}
//RECORRIENDO COMPONENTE MULTIMEDIA
$show_items_cm= 0;
if($deportes_app_car['items'] != null || $deportes_app['items'] != ""){
    foreach($deportes_app_car['items'] as $key => $val){
        if(isset($val['urls'][$formatvid]) && $val['urls'][$formatvid] != ""){
    		$url_video= $val['urls'][$formatvid];
            $ur_vid_aux= explode("?", $url_video);
            if(count($ur_vid_aux > 1)){
                $url_video= $ur_vid_aux[0];
            }
            $url_video= strtolower($url_video);
            $url_video= str_replace($subdominio, 'apps.',$url_video);
            
            if(isset($val['description']) && $val['description'] != ""){
        		$description= $val['description'];
        	}else{
        		$description= $val['title'];
        	}
            if(isset($val['publidate']) && $val['publidate'] != ""){
        		$date_pub= date('D, d M Y H:i:s ',strtotime($val['publidate']));
                $pubdate_unixt= strtotime($val['publidate']);
        	}else{
        		$date_pub= date("D, d M Y H:i:s");
                 $pubdate_unixt= time();
        	}
            if(isset($val['duration']) && $val['duration'] != ""){
        		$duration= $val['duration'];
        	}else{
        		$duration= "10";
        	}
			
			if(isset($val['still']) && $val['still'] != ""){
				$still= $val['still'];
			}else{
				$still= $val['thumb'];
			}
			$val_still= strpos($still, 'http://');
			if($val_still === false){
				$still= $dominio.$still;
			}
            $sepurl_still= explode("?", $still);
            if(count($sepurl_still) > 1){
                $still= $sepurl_still[0];
                $still= str_replace($subdominio, 'apps.',$still); 
            }
			
			$thumb= $val['thumb'];
			$val_thumb= strpos($thumb, 'http://');
			if($val_thumb === false){
				$thumb= $dominio.$thumb;
			}
            $sepurl_thumb= explode("?", $thumb);
            if(count($sepurl_thumb) > 1){
                $thumb= $sepurl_thumb[0];
                $thumb= str_replace($subdominio, 'apps.',$thumb); 
            }
                        
            $array_feed[$i]['unixtime']= $pubdate_unixt;
            $array_feed[$i]['section']= "carousel";
            $array_feed[$i]['ind_feed']= $key;
            $array_feed[$i]['title']= $val['title'];
            $array_feed[$i]['link']=  $val['urlpublic'];
            $array_feed[$i]['description']= $description;
            $array_feed[$i]['pubDate']= $date_pub;
            $array_feed[$i]['pubDateTime']= $date_pub;
            $array_feed[$i]['url_video']= $url_video;
            $array_feed[$i]['duration']= $duration;
            $array_feed[$i]['program']= 'deportes';
            $array_feed[$i]['role']= 'deportes';
            $array_feed[$i]['path']= $val['path'];
            $array_feed[$i]['keywords']= $val['keywords'];
            $array_feed[$i]['thumbnail']= $thumb;
            $array_feed[$i]['still']= $still;
            $array_feed[$i]['programid']= "0129";
            $array_feed[$i]['guid']= $val['guid'];
            $i++;                                                                
            $show_items_cm++;                                                   //contador dee elemntos en ComponenteMultimedia                
            
        }   
    }
}
//echo "<pre>"; print_r($array_feed); echo "</pre>";                            //rsort($array_feed);Ordenando elementos por timestamp
if($seccion == "home"){                                                         //Eliminando ultimos 3 items    :::     asociados al componente multimedia
    $array_feed_size= count($array_feed);
    unset($array_feed[$array_feed_size-3]);
    unset($array_feed[$array_feed_size-2]);
    unset($array_feed[$array_feed_size-1]);                                     //echo "<pre>"; print_r($array_feed); echo "</pre>";    //die();                                                                          
}
                                                                              
header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'> <rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom">';
echo "\n";
?>
<channel>
    <title>televisa.com</title>
    <link>http://www.televisa.com</link>
    <description><?=utf8_decode("El sitio número de internet de habla hispana con el mejor contenido de noticias, espectáculos, telenovelas, deportes, futbol, estadísticas y mucho más.")?></description>
    <image>
    	<title>televisa.com</title>
    	<url>http://i.esmas.com/img/univ/portal/rss/feed_1.jpg</url>
    	<link>http://www.televisa.com</link>
    </image>
    <language>es-mx</language>
    <copyright>2005 Comercio Mas S.A. de C.V</copyright>
    <managingEditor>ulises.blanco@esmas.net (Ulises Blanco)</managingEditor>
    <webMaster>feeds@esmas.com (feeds Esmas.com)</webMaster>
    <pubDate><?php echo date("D, d M Y H:i:s")?> GMT</pubDate>
    <lastBuildDate><?php echo date("D, d M Y H:i:s")?> GMT</lastBuildDate>
    <category>Home Principal esmas</category> 
    <generator>GALAXY 1.0</generator>
    <atom:link href="http://feeds.esmas.com/data-feeds-esmas/xml/index.xml" rel="self" type="application/rss+xml" />
    <ttl>60</ttl>
<?php   if($array_feed != "" || $array_feed != null){
            foreach($array_feed as $key_item => $value_element){    
                if($value_element['url_video'] != ""){      ?>
    <item>
        <title><![CDATA[<?=utf8_decode($value_element['title']);?>]]></title>
    	<link><?=$value_element['link'];?></link>
        <description><![CDATA[<?=utf8_decode($value_element['description'])?>]]></description>
    	<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
    	<pubDateTime><?=trim($value_element['pubDateTime'])?> CDT</pubDateTime> 
    	<media:content url='<?=$value_element['url_video']?>' fileSize='' type='video' medium='video' expression='full' duration='<?php echo $value_element['duration']?>' />
    	<media:title type='plain'><![CDATA[<?=utf8_decode($value_element['title']);?>]]></media:title>
    	<media:credit role='author' scheme='<?=$value_element['role'];?>'><![CDATA[<?php echo $value_element['path'];?>]]></media:credit>
    	<media:description type='plain'><![CDATA[<?=utf8_decode($value_element['description']);?>]]></media:description>
        <media:group>
            <media:content url='<?=$value_element['still'];?>' medium='image' height='360' width='640' />
    	</media:group>
        <media:keywords><![CDATA[<?=utf8_decode($value_element['keywords']);?>]]></media:keywords>
        <media:thumbnail url='<?=$value_element['thumbnail']?>'/>
    	<media:text>http://comentarios.esmas.com/feeds/comentarios.php?url=<?=$value_element['link']?></media:text>
    	<guid isPermaLink='false'><?=$value_element['guid']?></guid>			
    	<category><![CDATA[<?= $value_element['path'];?>]]></category>
    	<categoryid><?=$value_element['programid']?></categoryid>
    	<comments>http://comentarios.esmas.com/feeds/comentarios.php?url=<?php echo $value_element['link']?></comments>
    </item>        
<?php           }
            }  
        }       ?>
</channel>
</rss>
