<?php
//error_reporting(E_ALL ^ E_NOTICE);
/*
----- funciones
*/
function chars_specials( $str ){

  $str = str_replace( '&quot;', '"', $str ); 
  $str = str_replace( '&nbsp;', ' ', $str );  
  $str = str_replace( '&ldquo;', '', $str );
  $str = str_replace( '&rdquo;', '', $str );
  $str = str_replace( "&lsquo;", "'", $str );
  $str = str_replace( "&rsquo;", "'", $str );
  $str = str_replace( "?", "", $str );
  return $str; 
}
function convert_chars_to_entities( $str ) 
{ 
    $str = str_replace( 'ÃƒÂ¡', '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ©', '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ­', '&iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ³', '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂº', '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ±', '&ntilde;', $str ); 
    //$str = str_replace( '"', '&quot;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â¡' , '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â©' , '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â³' , '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Âº' , '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ¢Â€Â°', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â±', '&ntilde;', $str ); 
    $str = str_replace( 'ÃƒÂƒ-', '&iacute;', $str ); 
    $str = str_replace( 'ci?', 'ci&oacute;n', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‰', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ“', '&Oacute;', $str ); 
    $str = str_replace( 'ÃƒÂš', '&Uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‘', '&Ntilde;', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'a&ntilde;a', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'a&ntilde;e', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'a&ntilde;i', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'a&ntilde;o', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'a&ntildeu', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'e&ntilde;a', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'e&ntilde;e', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'e&ntildei', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'e&ntildeu', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'i&ntilde;a', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'i&ntilde;e', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'i&ntilde;i', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'i&ntilde;o', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'i&ntilde;u', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'o&ntilde;a', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'o&ntilde;e', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'o&ntilde;i', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'o&ntilde;u', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'u&ntilde;a', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'u&ntilde;e', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'u&ntilde;i', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'u&ntilde;o', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'u&ntilde;u', $str );
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½n', 'i&oacute;n', $str );
    $str = str_replace( 'Ã¡', '&aacute;', $str ); 
    $str = str_replace( 'Ã©', '&eacute;', $str ); 
    $str = str_replace( 'Ã­', '&iacute;', $str ); 
    $str = str_replace( 'Ã³', '&oacute;', $str ); 
    $str = str_replace( 'Ãº', '&uacute;', $str ); 
    $str = str_replace( 'Ã±', '&ntilde;', $str );
    $str = str_replace( 'Ã¼', '&uuml;', $str );  
    $str = str_replace( 'Ã', '&Aacute;', $str ); 
    $str = str_replace( 'Ãš', '&Uacute;', $str );
   
    return $str; 
}	
if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];    
}else{
    $seccion = "home";
}
switch ($seccion) {
	case 'home':
		$req_mix = 'http://deportes.televisa.com/content/televisa/deportes.mix.js';
        $req_carousel= 'http://deportes.televisa.com/content/televisa/deportes.mphoto.js';
        break;
	case 'futbol':
		$req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol.photo.js';
		break;
    case 'seleccion':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/seleccion-mexicana.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/seleccion-mexicana.photo.js';
		break;
    case 'fut_mexicano':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol-mexicano.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol-mexicano.photo.js';
		break;
    case 'fut_internacional':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/futbol-internacional.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/futbol-internacional.photo.js';
		break;       
    case 'boxeo':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/boxeo.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/boxeo.photo.js';		
		break; 
    case 'nba':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/nba-basquetbol.mix.js';
		$req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/nba-basquetbol.photo.js';
		break; 
    case 'beisbol':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/beisbol.mix.js';
		$req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/beisbol.photo.js';
		break; 
    case 'nfl':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/nfl-futbol-americano.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/nfl-futbol-americano.photo.js';
		break; 
    case 'mas_deporte':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/mas-deportes.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/mas-deportes.photo.js';
		break; 
    case 'tdn':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tdn.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tdn.photo.js';
		break; 
    case 'autos':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/autos.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/autos.photo.js';
		break;     
    case 'golf':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/golf.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/golf.photo.js';
		break;     
    case 'tenis':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tenis.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tenis.photo.js';
		break;     
    case 'tdw':
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/tdw.mix.js';
        $req_carousel = 'http://deportes.televisa.com/content/televisa/deportes/tdw.photo.js';
		break;                
}
$dom_feed= explode("content/televisa", $req_mix);
$dominio= trim($dom_feed[0], "/");
$subdominio= array("m4v.","media.","flash.");

$cURL = curl_init($req_mix);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$app_deo = curl_exec($cURL);
#$app_deo = utf8_encode(curl_exec($cURL));
$deportes_app = json_decode($app_deo, true);                                //echo "<pre>"; print_r($deportes_app); echo "</pre>";die();

$cURL_car = curl_init($req_carousel);
curl_setopt($cURL_car,CURLOPT_RETURNTRANSFER, TRUE);
$app_car = curl_exec($cURL_car);
#$app_car = utf8_encode(curl_exec($cURL_car));
$deportes_app_car = json_decode($app_car, true);                           //echo "<pre>"; print_r($deportes_app_car); echo "</pre>";die();
$array_feed_photo = array();
$i= 0;
if($deportes_app['items'] != null || $deportes_app['items'] != ""){
    foreach($deportes_app['items'] as $key => $val){
	    if ($val['typeElement']=="photo"){
			   $array_feed_photo[$key]['title']= $val['title'];
			   $array_feed_photo[$key]['typeElement']= $val['typeElement'];
			   
			   $rlink = strpos($val['link'], 'http://');
			   if($rlink !== false){
					$link = $val['link']; 
			   } else {
					$link = $dominio.$val['link'];
			   }  			   
			   $array_feed_photo[$key]['link'] = $link;
			   $array_feed_photo[$key]['description'] = $val['description'];
			   if ( !isset($val['pubDate']) || $val['pubDate'] == '' || $val['pubDate'] == NULL || empty($val['pubDate']) ){
				   $pubDate = date('D, d M Y H:i:s ',strtotime(date("Y/m/d")));		
			   } else {
				   $pubDate = date('D, d M Y H:i:s ',strtotime($val['pubDate']));
			   }			   
			   $array_feed_photo[$key]['pubDate'] = $pubDate;
			   $rthum = strpos($val['thumbnail'], 'http://');
			   if($rthum !== false){
					$thumb = $val['thumbnail']; 
			   } else {
					$thumb = $dominio.$val['thumbnail'];
			   } 			   
			   $array_feed_photo[$key]['thumbnail'] = $thumb; 
			   $array_feed_photo[$key]['photos'] = $val['photo'];
			   $array_feed_photo[$key]['guid'] = $val['guid'];
               $array_feed_photo[$key]['section'] = 'mix';
	   
	   }  
    }
}
if($deportes_app_car['items'] != null || $deportes_app['items'] != ""){
    foreach($deportes_app_car['items'] as $key => $val){
			   $array_feed_photo[$key]['title']= $val['title'];
			   $array_feed_photo[$key]['typeElement']= $val['typeElement'];
			   
			   $rlink = strpos($val['link'], 'http://');
			   if($rlink !== false){
					$link = $val['link']; 
			   } else {
					$link = $dominio.$val['link'];
			   }  			   
			   $array_feed_photo[$key]['link'] = $link;
			   
               if(isset($val['description']) && $val['description'] != ""){
            		$array_feed_photo[$key]['description']= $val['description'];
   	           }else{
            		$array_feed_photo[$key]['description']= $val['title'];
     	       }
               
               
			   if ( !isset($val['pubDate']) || $val['pubDate'] == '' || $val['pubDate'] == NULL || empty($val['pubDate']) ){
				   $pubDate = date('D, d M Y H:i:s ',strtotime(date("Y/m/d")));		
			   } else {
				   $pubDate = date('D, d M Y H:i:s ',strtotime($val['pubDate']));
			   }			   
			   $array_feed_photo[$key]['pubDate'] = $pubDate;
			   $rthum = strpos($val['thumbnail'], 'http://');
			   if($rthum !== false){
					$thumb = $val['thumbnail']; 
			   } else {
					$thumb = $dominio.$val['thumbnail'];
			   } 			   
			   $array_feed_photo[$key]['thumbnail'] = $thumb; 
			   $array_feed_photo[$key]['photos'] = $val['photo'];
			   $array_feed_photo[$key]['guid'] = $val['guid'];
               $array_feed_photo[$key]['section'] = 'carousel';
	   
    }
}
//echo "<pre>"; print_r($array_feed_photo);echo "</pre>";die();
if($seccion == "home"){                                                         //Eliminando ultimos 3 items    :::     asociados al componente multimedia
    $array_feed_size= count($array_feed_photo);
    unset($array_feed_photo[$array_feed_size-3]);
    unset($array_feed_photo[$array_feed_size-2]);
    unset($array_feed_photo[$array_feed_size-1]);                               //echo "<pre>"; print_r($array_feed_photo); echo "</pre>";    //die();                                                                          
}

header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'> <rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom">';
echo "\n";
?>
<channel>
    <title>televisa.com</title>
    <link>http://www.televisa.com</link>
    <description><?=utf8_decode("El sitio número de internet de habla hispana con el mejor contenido de noticias, espectáculos, telenovelas, deportes, futbol, estadísticas y mucho más.")?></description>
    <image>
    	<title>televisa.com</title>
    	<url>http://i.esmas.com/img/univ/portal/rss/feed_1.jpg</url>
    	<link>http://www.televisa.com</link>
    </image>
    <language>es-mx</language>
    <copyright>2005 Comercio Mas S.A. de C.V</copyright>
    <managingEditor>ulises.blanco@esmas.net (Ulises Blanco)</managingEditor>
    <webMaster>feeds@esmas.com (feeds Esmas.com)</webMaster>
    <pubDate><?=date("D, d M Y H:i:s")?> GMT</pubDate>
    <lastBuildDate><?=date("D, d M Y H:i:s")?> GMT</lastBuildDate>
    <category>Home Principal esmas</category> 
    <generator>GALAXY 1.0</generator>
    <atom:link href="http://feeds.esmas.com/data-feeds-esmas/xml/index.xml" rel="self" type="application/rss+xml" />
    <ttl>60</ttl>
<?php   if($array_feed_photo != "" || $array_feed_photo != null){
          
			foreach($array_feed_photo as $key_item => $value_element){

			  if(isset($value_element['photos']) && count($value_element['photos']) > 0 && $value_element['photos'] <> 'empty' && $value_element['photos'] <> '' && $value_element['photos'] <> NULL ){  
			?>
		<item>
	        <title><![CDATA[<?=chars_specials(utf8_decode($value_element['title'])); ?>]]></title>
            <link><?=$value_element['link']?></link>
            <description><![CDATA[<?=chars_specials(utf8_decode($value_element['description']))?>]]></description>
			<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
			<media:thumbnail url='<?=utf8_decode($value_element['thumbnail']);?>' /> 
<?php 
foreach($value_element['photos'] as $k => $valor){
if(isset($valor['images']) && count($valor['images']) > 0){    
    if(isset($valor['title']) &&  $valor['title'] <> "" &&  $valor['title'] <> NULL){
         $title_gal = $valor['title'];
    }else{
         $title_gal = "...";
      }?>	 
            <media:group>
	          <media:title><![CDATA[<?=chars_specials(utf8_decode($title_gal))?>]]></media:title>			 
<?php
    	      $images = $valor['images'];
 			    foreach($images as $m => $imagen){
				   $p = strpos($m, 'x', 0);
				   $h = substr($m,$p+1); 
				   $w = substr($m,0,$p);
			       $rimg = strpos($images[$m], 'http://');
			       if($rimg !== false){ $imagen = $images[$m]; } else {  $imagen = $dominio.$images[$m]; }
				   ?>
                    <media:content url='<?=utf8_decode($imagen);?>' medium='image' width='<?=$w?>' height='<?=$h?>' />
                <?php
				}
				?>			 
			</media:group>	
<?php
    }
}
?>		<guid isPermaLink='false'><?=$value_element['guid']?></guid>			 
        </item>		      
<?php           
            } 
          } 			
        }       ?>
</channel>
</rss>
